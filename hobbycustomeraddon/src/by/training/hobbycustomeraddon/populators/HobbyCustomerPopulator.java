package by.training.hobbycustomeraddon.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import org.apache.log4j.Logger;

public class HobbyCustomerPopulator extends CustomerPopulator implements Populator<CustomerModel, CustomerData> {
    static final Logger LOGGER = Logger.getLogger(HobbyCustomerPopulator.class);

    @Override
    public void populate(CustomerModel source, CustomerData target) {
        String hobby = source.getHobby();
        if (hobby != null) {
            target.setHobby(hobby);
        }
        super.populate(source, target);
    }
}
