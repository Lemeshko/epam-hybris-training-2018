package by.training.hobbycustomeraddon.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.log4j.Logger;

public class HobbyCustomerReversePopulator extends CustomerReversePopulator implements Populator<CustomerData, CustomerModel> {
    static final Logger LOGGER = Logger.getLogger(HobbyCustomerReversePopulator.class);

    @Override
    public void populate(CustomerData source, CustomerModel target) throws ConversionException {
        super.populate(source, target);
        String hobby = source.getHobby();
        target.setHobby(hobby);
    }
}
