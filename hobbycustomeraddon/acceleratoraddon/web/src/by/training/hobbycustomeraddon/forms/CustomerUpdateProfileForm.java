package by.training.hobbycustomeraddon.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;

public class CustomerUpdateProfileForm extends UpdateProfileForm {
    private String hobby;

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
}
